<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{

    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getCharacters(): array
    {
        $response = $this->client->request(
            'GET',
            'https://rickandmortyapi.com/api/character'
        );

        return $response->toArray();
    }

    public function getNextCharacters(): array
    {
        $response = $this->client->request(
            'GET',
            'https://rickandmortyapi.com/api/character?page=2'
        );

        return $response->toArray();
    }

    public function index(CallApiService $callApiService): Response
    {
        dd($callApiService->getCharacters());

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}